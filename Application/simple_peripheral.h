/******************************************************************************

 @file       simple_peripheral.h

 @brief This file contains the Simple Peripheral sample application
        definitions and prototypes.

 Group: CMCU, SCS
 Target Device: CC2640R2

 ******************************************************************************
 
 Copyright (c) 2013-2018, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 Release Name: simplelink_cc2640r2_sdk_02_20_00_49
 Release Date: 2018-07-16 13:19:56
 *****************************************************************************/

#ifndef SIMPLEPERIPHERAL_H
#define SIMPLEPERIPHERAL_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
*  EXTERNAL VARIABLES
*/

/*********************************************************************
 * CONSTANTS
 */

#define BLE_NVID_CUST_START 0x80 //!< Start of the Customer's NV IDs
//#define BLE_NVID_CUST_END 0xF5 //!< End of the Customer's NV IDs

#define COM_OPEN               /*0x21*/0x61
#define COM_CLOSE              /*0x22*/0x62
#define COM_SETCLOCKWISE       0x50
#define COM_SETCCLOCKWISE      0x51
#define COM_GETSTATUS          0x24
#define COM_GETTYPE            0x29
#define COM_KEYWRITE           0x30
#define COM_INIT               0x36
#define COM_FUPDATE            0x37
#define COM_KEYREAD            0x31
#define COM_KEYERASE           0x32
#define COM_LOGREAD            0x35
#define COM_KEYERASEALL        0x44
#define COM_LOGERASE           0x46
#define COM_SETTIME            0x55
#define COM_GETTIME            0x56

#define KEYS_MAX_NUM           8
#define KEY_LEN                8
#define SNV_BEGIN              0x80
#define SNV_LOG_BEGIN          0x90
#define INTFLASH_ADDR          0x0

#define COM_FAIL               0xFF
#define COM_WRONGMASTERKEY     0xF0
#define COM_NOMASTERKEY        0xF1
#define COM_WRONGKEYID         0xF2
#define COM_WRONGKEY           0xF3
#define COM_WRONGLOGID         0xF4
#define COM_FAILINIT           0xF5



/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task creation function for the Simple Peripheral.
 */
extern void SimplePeripheral_createTask(void);


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* SIMPLEPERIPHERAL_H */
